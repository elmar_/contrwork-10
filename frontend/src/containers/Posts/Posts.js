import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import {useDispatch, useSelector} from "react-redux";
import {deleteNews, fetchNews} from "../../store/actions";
import Post from "../../components/Post/Post";
import Spinner from "../../components/Spinner/Spinner";

//        import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles(theme => ({
    titlesBlock: {
        "display": 'flex',
        'justifyContent': 'space-between'
    },
    divider: {
        "marginBottom": theme.spacing(2),
        "marginTop": theme.spacing(2)
    }

}));

const Posts = props => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const posts = useSelector(state => state.news);
    const loading = useSelector(state => state.newsLoading);


    useEffect(() => {
        dispatch(fetchNews());
    }, [dispatch]);

    const delNews = id => {
        dispatch(deleteNews(id));
    };

    let news = (
        posts.map(el => (
            <Post datetime={el.datetime} id={el.id} image={el.image} title={el.title} delNews={delNews} key={el.id} />
        ))
    );

    if (loading) {
        news = <Spinner />
    }

    return (
        <div>
            <Box className={classes.titlesBlock}>
                <Typography variant="h4">Posts</Typography>
                <Button variant="contained" color="primary" onClick={() => props.history.push('/addPost')}>
                    Add new post
                </Button>
            </Box>
            <Divider className={classes.divider} />
            <div>
                {news}
            </div>
        </div>
    );
};

export default Posts;