import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPost} from "../../store/actions";
import {apiURL} from "../../apiURL";

const News = props => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.post);

    useEffect(() => {
        const fetch = async () => {
            await dispatch(fetchPost(props.match.params.id));
        };
        fetch().catch(console.error);
    }, [dispatch, props.match.params.id]);

    return (
        <div className="News">
            <h3>{post.title}</h3>
            <p>{post.datetime}</p>
            <p>{post.content}</p>
            {post.image ? <img src={apiURL + '/uploads/' + post.image} alt="#" width="900" height="auto"/> : <p>no image</p>}
            <h4>Comments</h4>
            <p>не успел (</p>
        </div>
    );
};

export default News;