import React from 'react';
import Form from "../../components/Form/Form";
import {useDispatch} from "react-redux";
import {postNews} from "../../store/actions";

const AddPost = () => {
    const dispatch = useDispatch();

    const onSubmit = data => {
        dispatch(postNews(data));
    };

    return (
        <div>
            <h3>Add new post</h3>
            <Form onSubmit={onSubmit} />
        </div>
    );
};

export default AddPost;