import React from 'react';
import Header from "./components/Header/Header";
import Posts from "./containers/Posts/Posts";
import {Route, Switch} from "react-router-dom";
import AddPost from "./containers/AddPost/AddPost";
import News from "./containers/News/News";
import Container from "@material-ui/core/Container";

const App = () => {
    return (
        <>
            <Header />
            <main>
                <Container>
                    <Switch>
                        <Route path="/" exact component={Posts} />
                        <Route path="/addPost" component={AddPost} />
                        <Route path="/news/:id" component={News} />
                    </Switch>
                </Container>
            </main>
        </>
    );
};

export default App;