import axiosApi from "../axios-api";

export const POST_NEWS_REQUEST = 'POST_NEWS_REQUEST';
export const POST_NEWS_SUCCESS = 'POST_NEWS_SUCCESS';
export const POST_NEWS_ERROR = 'POST_NEWS_ERROR';

export const FETCH_NEWS_REQUEST = 'FETCH_NEWS_REQUEST';
export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const FETCH_NEWS_ERROR = 'FETCH_NEWS_ERROR';

export const DELETE_NEWS_REQUEST = 'DELETE_NEWS_REQUEST';
export const DELETE_NEWS_SUCCESS = 'DELETE_NEWS_SUCCESS';
export const DELETE_NEWS_ERROR = 'DELETE_NEWS_ERROR';

export const FETCH_POST_REQUEST = 'FETCH_POST_REQUEST';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_ERROR = 'FETCH_POST_ERROR';

export const fetchPostRequest = () => ({type: FETCH_POST_REQUEST});
export const fetchPostSuccess = post => ({type: FETCH_POST_SUCCESS, post});
export const fetchPostError = () => ({type: FETCH_POST_ERROR});

export const deleteNewsRequest = () => ({type: DELETE_NEWS_REQUEST});
export const deleteNewsSuccess = id => ({type: DELETE_NEWS_SUCCESS, id});
export const deleteNewsError = () => ({type: DELETE_NEWS_ERROR});

export const postNewsRequest = () => ({type: POST_NEWS_REQUEST});
export const postNewsSuccess = () => ({type: POST_NEWS_SUCCESS});
export const postNewsError = () => ({type: POST_NEWS_ERROR});

export const fetchNewsRequest = () => ({type: FETCH_NEWS_REQUEST});
export const fetchNewsSuccess = news => ({type: FETCH_NEWS_SUCCESS, news});
export const fetchNewsError = () => ({type: FETCH_NEWS_ERROR});



export const postNews = data => {
    return async dispatch => {
        try {
            dispatch(postNewsRequest());
            await axiosApi.post('/news', data);
            dispatch(postNewsSuccess());
        } catch (e) {
            dispatch(postNewsError());
        }
    };
};


export const fetchNews = () => {
    return async dispatch => {
        try {
            dispatch(fetchNewsRequest());
            const response = await axiosApi.get('/news');
            dispatch(fetchNewsSuccess(response.data));
        } catch(e) {
            dispatch(fetchNewsError());
        }
    };
};

export const deleteNews = id => {
    return async dispatch => {
        try {
            dispatch(deleteNewsRequest());
            await axiosApi.delete('/news/' + id);
            dispatch(deleteNewsSuccess(id));
        } catch (e) {
            dispatch(deleteNewsError());
        }
    };
};

export const fetchPost = id => {
    return async dispatch => {
        try {
            dispatch(fetchPostRequest());
            const response = await axiosApi.get('/news/' + id);
            dispatch(fetchPostSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostError());
        }
    };
};