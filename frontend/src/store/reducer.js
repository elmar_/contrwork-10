import {
    DELETE_NEWS_ERROR,
    DELETE_NEWS_REQUEST,
    DELETE_NEWS_SUCCESS,
    FETCH_NEWS_ERROR,
    FETCH_NEWS_REQUEST,
    FETCH_NEWS_SUCCESS,
    FETCH_POST_SUCCESS
} from "./actions";

const initialState = {
    news: [],
    post: {},
    newsLoading: false,
    newsError: false,
    deleteNewsError: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_SUCCESS:
            return {...state, news: action.news, newsLoading: false};
        case FETCH_NEWS_REQUEST:
            return {...state, newsLoading: true};
        case FETCH_NEWS_ERROR:
            return {...state, newsLoading: false, newsError: true};
        case DELETE_NEWS_REQUEST:
            return {...state, newsLoading: true};
        case DELETE_NEWS_SUCCESS:
            const newsCopy = [...state.news];
            const index = newsCopy.findIndex(element => element.id === action.id);
            newsCopy.splice(index, 1);
            return {...state, newsLoading: false, news: [...newsCopy]};
        case DELETE_NEWS_ERROR:
            return {...state, newsLoading: false, deleteNewsError: true};
        case FETCH_POST_SUCCESS:
            return {...state, post: action.post};
        default:
            return state;
    }
};

export default reducer;