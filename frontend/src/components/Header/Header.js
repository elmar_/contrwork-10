import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import {makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";

const useStyles = makeStyles(theme => ({
    bar: {
        "marginBottom": theme.spacing(3)
    },
    logo: {
        "textDecoration": 'none',
        "color": "white"
    }
}));

const Header = () => {
    const classes = useStyles();

    return (
        <>
            <CssBaseline />
                <AppBar>
                    <Toolbar>
                        <Typography variant="h5"><Link to="/" className={classes.logo}>News</Link></Typography>
                    </Toolbar>
                </AppBar>
            <Toolbar className={classes.bar} />
        </>
    );
};

export default Header;