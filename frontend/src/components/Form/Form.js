import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import FileInput from "../FileInput/FileInput";

const Form = ({onSubmit}) => {

    const [state, setState] = useState({
        title: '',
        content: '',
        image: ''
    });

    const onChangeForm = e => {
        const {name, value} = e.target;

        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setState(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const onSubmitHandler = e => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        setState({
            title: '',
            content: '',
            image: ''
        });
        onSubmit(formData);
    };


    return (
        <form onSubmit={onSubmitHandler}>
            <Grid container spacing={2} direction="column">
                <Grid item xs>
                    <TextField
                        required
                        fullWidth
                        label="Title"
                        variant="outlined"
                        name="title"
                        value={state.title}
                        onChange={onChangeForm}
                    />
                </Grid>
                <Grid item xs>
                    <FileInput
                        onChange={fileChangeHandler}
                        name="image"
                        label="Image"
                    />
                </Grid>
                <Grid item xs>
                    <TextField
                        required
                        fullWidth
                        label="Content"
                        variant="outlined"
                        name="content"
                        multiline
                        rows={3}
                        value={state.content}
                        onChange={onChangeForm}
                    />
                </Grid>
                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained">Send</Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default Form;