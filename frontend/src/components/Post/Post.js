import React from 'react';
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import './Post.css';
import {apiURL} from "../../apiURL";


const Post = ({title, datetime, image, id, delNews}) => {

    return (
        <div className="Post">
            <div className="img">
                {image ? <img src={apiURL + '/uploads/' + image} alt="#" className="image"/> : <p>no image</p>}
            </div>
            <div className="contentBox">
                <p className="title">{title}</p>
                <div className="bottomBox">
                    <p>At {datetime}</p>
                    <Link to={'/news/' + id} className='link'>Read full post</Link>
                    <Button
                        onClick={() => delNews(id)}
                        variant="contained"
                        color="secondary"
                    >
                        Delete
                    </Button>
                </div>
            </div>
        </div>
    );
};

export default Post;