const express = require('express');
const fs = require('fs').promises;


const fileName = 'news.json';

let data = [];

module.exports = {
    async init() {
        try {
            const fileContent = await fs.readFile(fileName);
            data = JSON.parse(fileContent);
        } catch (e) {
            data = [];
        }
    },
    async getNews() {
        return data;
    },
    async addNews(news) {
        data.push(news);
        await this.save();
    },
    async delete(id) {
        const index = data.findIndex(ob => {
            return ob.id === id;
        });

        if (index > -1) {
            data.splice(index, 1);
            await this.save();
            return {message: 'news was deleted'};
        } else {
            return {message: 'no such id'};
        }
    },
    async findById(id) {
        const response = data.find(ob => {
            if (ob.id === id) {
                return ob;
            }
        });

        return response;
    },
    async save() {
        await fs.writeFile(fileName, JSON.stringify(data, null, 2));
    }
}
