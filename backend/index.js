const express = require('express');
const cors = require('cors');
const news = require('./app/news');

const fileNews = require('./files/fsNews');

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = '8000';


app.use('/news', news);


const run = async () => {

    await fileNews.init();

    app.listen(port, () => {
        console.log('we are on port ' + port);
    });
};

run().catch(console.error);
