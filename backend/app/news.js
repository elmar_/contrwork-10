const express = require('express');
const {nanoid} = require('nanoid');
const path = require('path');
const multer = require('multer');
const config = require('../config');
const router = express.Router();

const fileWork = require('../files/fsNews');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.post('/', upload.single('image'), async (req, res) => {

    const news = req.body;

    if (req.file) {
        news.image = req.file.filename;
    }

    if (news.image === "undefined") {
        news.image = '';
    }

    if (!news.title) {
        return res.status(404).send({
            message: 'no title'
        });
    }

    if (!news.content) {
        return res.status(404).send({
            message: 'no content'
        });
    }

    news.datetime = new Date().toLocaleString();
    news.id = nanoid();
    await fileWork.addNews(news);
    res.send(news);
});

router.get('/', async (req, res) => {
    const data = await fileWork.getNews();
    res.send(data);
});

router.get('/:id', async (req, res) => {
    console.log(req.params.id)
    const response = await fileWork.findById(req.params.id);
    console.log(response)

    if (response) {
        res.send(response);
    } else {
        res.status(404).send({message: 'cant find id'});
    }
});

router.delete('/:id', async (req, res) => {
    const response = await fileWork.delete(req.params.id);
    res.send(response);
})



module.exports = router;